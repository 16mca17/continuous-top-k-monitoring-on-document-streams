﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TopkMonitoring
{
    public partial class index : System.Web.UI.Page
    {
        DbCon dbcobj = new DbCon();
        bool flag;
        protected void Page_Load(object sender, EventArgs e)
        {
            flag = false;
            lb_name.Visible = false;
            lb_uname.Visible = false;
            lb_pass.Visible = false;
            lb_confirm.Visible = false;
            lb_compare.Visible = false;
        }

        protected void btn_register_Click(object sender, EventArgs e)
        {

            validation obj = new validation();
            if (!obj.check_name(txt_name.Text))
            {
                lb_name.Visible = true;
                lb_name.Text = obj.error;
                flag = true;
                
            }
            if (!obj.check_username(txt_uname.Text))
            {
                lb_uname.Visible = true;
                lb_uname.Text = obj.error;
                flag = true;
            }
            if (!obj.check_password(txt_pass.Text))
            {
                lb_pass.Visible = true;
                lb_pass.Text = obj.error;
                flag = true;
            }
            //if (!obj.check_confpassword(txt_confirm.Text))
            //{
            //    lb_confirm.Visible = true;
            //    lb_confirm.Text = obj.error;
            //    flag = true;
            //}
           
            if (!obj.compare_password(txt_pass.Text, txt_confirm.Text))
            {
                lb_compare.Visible = true;
                lb_compare.Text = obj.error;
                flag = true;

            }

            if (flag==false)
            {
                int u_id;
                dbcobj.ExecuteQuery("Select max(U_id) from user_table");
                if (dbcobj.dt.Rows.Count > 0 && dbcobj.dt.Rows[0][0].ToString() != "")
                {
                    u_id = Convert.ToInt32(dbcobj.dt.Rows[0][0].ToString()) + 1;
                }
                else
                {
                    u_id = 1;

                }
                dbcobj.ExecuteNonQuery("insert into user_table values('" + u_id + "','" + txt_uname.Text + "','" + txt_pass.Text + "','" + txt_name.Text + "')");
            }
            //txt_uname.Text = "";
            //txt_pass.Text = "";
            //txt_confirm.Text = "";
            //txt_name.Text = "";
        }
    }
}