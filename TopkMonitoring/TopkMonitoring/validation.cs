﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;

namespace TopkMonitoring
{
    public class validation
    {
        public string error;
        public Boolean check_name(string txtname)
        {
            if (txtname == "")
            {
                error = "Name cannot be blank";
                return false;
            }
            
            else if (txtname.Length < 4)
            {
                error = "Name should contain minimum 4 letters";
                return false;
            }
            else
                return true;
        }
        public Boolean check_age(string txtage,int min=18,int max=50)
        {
            if (txtage == "")
            {
                error = "Age cannot be blank";
                return false;
            }
            int number;
            if (!(int.TryParse(txtage, out number)))
                {
                error = "Enter valid age";
                return false;
            }
            
            else if ((number < min) || (number > max))
            {
                error = "Age must be between 18 and 60";
                return false;
            }
            else
            return true;
        }
        public Boolean check_email(String txtemail)
        {
            if(txtemail=="")
            {
                error = "Email cannot be blank";
                return false;
            }
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regex.Match(txtemail);
            if (!match.Success)
            {
                error = "Please enter proper email";
                return false;
            }
            else
                return true;
        }
        public Boolean check_mobile(String txtmob)
        {
            if(txtmob=="")
            {
                error = "Mobile number cannot be blank";
                return false;
            }
            if((txtmob.Length<10)||(txtmob.Length>10))
            {
                error = "Enter 10 digits";
                return false;
            }
            Regex regex = new Regex(@"^[9876]{1}[0-9]{9}$");
            Match match = regex.Match(txtmob);
            if (!match.Success)
            {
                error = "Please enter proper mobile number";
                return false;
            }
            else
                return true;
        }
        public Boolean check_password(string txtpassword)
        {
            if (txtpassword == "")
            {
                error = "Password cannot be blank";
                return false;
            }
            Regex regex = new Regex(@"^(?=.*[0-9])(?=.*[!@#$%^&*])[0-9a-zA-Z!@#$%^&*0-9]{8,}$");
            Match match = regex.Match(txtpassword);
            if (!match.Success)
            {
                error = "Please enter a strong password";
                return false;
            }
            else
                return true;
        }
        public Boolean check_confpassword(string txtconfpassword)
        {
            if (txtconfpassword == "")
            {
                error = "Confirm password cannot be blank";
                return false;
            }
            else
                return true;
        }

        //  public Boolean check_confpassword(string txtconfpassword)
        // {
        //     if (txtconfpassword == "")
        //     {
        //         error = "Confirm password cannot be blank";
        //         return false;
        //     }
        //     else
        //         return true;
        // }
        public Boolean compare_password(string psd,string confpsd)
        {
            if (psd != confpsd)
            {
                error = "Passwords not matching";
                return false;
            }
            else
                return true;

        }
        public Boolean check_username(string txtusername)
        {

            if (txtusername == "")
            {
                error = "Username cannot be null ";

                return false;
            }
            Regex regex = new Regex("^[a-zA-Z0-9]+$");
            Match match = regex.Match(txtusername);
            if (!match.Success)
            {
                error = "Username should contain atleast one letter or number";
                return false;
            }
            else
                return true;
        }

    }
}