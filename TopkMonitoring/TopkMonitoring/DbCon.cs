﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;


namespace TopkMonitoring
{
    public class DbCon
    {
        public SqlCommand cmd;
        public DataSet ds = new DataSet();
        public static string connstring = System.Configuration.ConfigurationManager.ConnectionStrings["DbConn"].ConnectionString;
        public SqlConnection conn = new SqlConnection(connstring);
        public SqlDataAdapter adpt;
        public DataTable dt = new DataTable();
        public string error;

        public void ExecuteQuery(string strquery)
        {
            SqlCommand cmd = new SqlCommand(strquery, conn);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            try
            {
                sda.Fill(ds);
                sda.Fill(dt);
            }
            catch (Exception ex) {
                error=ex.Message;
            }
            conn.Close();
        }

        // public void ExecuteQuery(string strquery)
        // {
        //     SqlCommand cmd = new SqlCommand(strquery, conn);
        //     SqlDataAdapter sda = new SqlDataAdapter(cmd);
        //     try
        //     {
        //         sda.Fill(ds);
        //         sda.Fill(dt);
        //     }
        //     catch (Exception ex) {
        //         error=ex.Message;
        //     }
        //     conn.Close();
        // }



        public void ExecuteNonQuery(string strquery)
        {
            try
            {
                conn.Open();
                SqlCommand osc = new SqlCommand(strquery, conn);
                osc.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }
            conn.Close();
        }
       
      
        
       

    }
}