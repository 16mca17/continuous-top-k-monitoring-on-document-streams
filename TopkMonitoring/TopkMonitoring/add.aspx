﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="add.aspx.cs" Inherits="TopkMonitoring.add" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title>Home</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/font-awesome.css" rel="stylesheet" />
    <link href="css/custom.css" rel="stylesheet" />
    <link href="css/animate.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">

        <div id="wrapper">

            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav metismenu" id="side-menu">
                        <li>
                            <a href="add.aspx"><span class="nav-label">Add </span></a>

                        </li>
                        <li>
                            <a href="view.aspx"><span class="nav-label">View </span></a>
                        </li>
<!-- 
                        <li>
                            <a href="view.aspx"><span class="nav-label">View </span></a>
                        </li> -->

                        <li>
                            <a href="search.aspx"><span class="nav-label">Search </span></a>
                        </li>
                    </ul>
                </div>
            </nav>

            <div id="page-wrapper" class="gray-bg">
                <div class="row border-bottom">
                    <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                        <div class="navbar-header">
                        </div>
                        <ul class="nav navbar-top-links navbar-right">
                            <li>
                                <a href="login.aspx">Log out
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>

                <div class="row">
                    <div class="col-lg-12 animated fadeInRight">
                        <div class="mail-box-header">
                            <h2>Add</h2>
                        </div>
                        <div class="mail-box">
                        </div>
                    </div>
                </div>

               
                <div class="col-lg-12" style="display:none" id="success" runat="server">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Example with fullscreen option</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="fullscreen-link">
                                <i class="fa fa-expand"></i>
                            </a>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">

                        <p id="msg">
                            File uploaded successfully
                           </p>
                      
                    </div>
                </div>
            </div>

                  <div class="col-lg-12" style="display:none" id="error" runat="server">
                <div class="ibox">
                    <div class="ibox-title">
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="fullscreen-link">
                                <i class="fa fa-expand"></i>
                            </a>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">

                        <p >
                           Please choose a file
                           </p>
                      
                    </div>
                </div>
            </div>
                <div>
                <div class="form-group">
                    <asp:Label ID="Label1" runat="server" Text="Document name:"></asp:Label>
                    <asp:TextBox ID="doc_name" class="form-control"  runat="server" Width="400px"></asp:TextBox>
                    </div>
                     <div class="form-group">
                    <asp:Label ID="Label2" runat="server" Text="Category"></asp:Label>
                    <asp:TextBox ID="txt_cat" class="form-control"  runat="server" Width="400px"></asp:TextBox>
                    </div>
                     <div class="form-group">
                    <asp:Label ID="Label3" runat="server" Text="Tags:"></asp:Label>
                    <asp:TextBox ID="txt_tags" class="form-control"  runat="server" Width="400px"></asp:TextBox>
                    </div>
                <div class="form-group">
                    <asp:Label ID="Label4" runat="server" Text="File name:"></asp:Label> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                    <asp:fileupload ID="fileupload1" runat="server" Width="211px"></asp:fileupload>
                     <%--<asp:Button ID="btn_upload" runat="server" Text="Upload" class="btn btn-primary" OnClick="btn_upload_Click" OnClientClick="showdiv1()" />--%>
                </div>
                    <asp:Button ID="btn_submit" runat="server" Text="Submit" class="btn btn-primary" OnClick="btn_submit_Click" />
            </div>
           </div>

            <div class="footer">
               
            </div>

        </div>

        <script src="js/jquery-3.1.1.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/jquery.metisMenu.js"></script>
        <script src="js/jquery.slimscroll.min.js"></script>
        <script src="js/inspinia.js"></script>
        <script src="js/pace.min.js"></script>
        <script src="js/icheck.min.js"></script>
        <script>
            $(document).ready(function () {
                $('.i-checks').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green',
                });
            });
        </script>
        <script type="text/javascript">
            function showdiv1() {
                document.getElementById("success").style.display = 'block';
                return false;
            }
           
        </script>

    </form>
</body>
</html>


