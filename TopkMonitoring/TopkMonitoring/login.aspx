﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="TopkMonitoring.login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   

     <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <title>TopkMonitoring | Login</title>

    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/font-awesome.css" rel="stylesheet" />
    <link href="css/animate.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet" />

</head>
<body class="white-bg">
    <form id="form1" runat="server">
    
     <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>

                <h1 class="logo-name">IN+</h1>

            </div>
            <h3>Welcome to TopkMonitoring</h3>
           
                <div class="form-group">
                    <asp:TextBox ID="txt_uname" runat="server" class="form-control" placeholder="Username" required=""></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:TextBox ID="txt_pass" runat="server" class="form-control" placeholder="Password" required=""></asp:TextBox>
                </div>
                <asp:Button ID="btn_login" runat="server" Text="Login" type="submit" class="btn btn-primary block full-width m-b" OnClick="btn_login_Click" />
               

                <%--<a href="#"><small>Forgot password?</small></a>--%>
                <p class="text-muted text-center"><small>Do not have an account?</small></p>
                <!-- <p class="text-muted text-center"><small>Do not have an account?</small></p> -->
                <a class="btn btn-sm btn-white btn-block" href="index.aspx">Create an account</a>

                
         
        </div>
    </div>


        <script src="js/jquery-3.1.1.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.js"></script>
    
    </form>
</body>
</html>
