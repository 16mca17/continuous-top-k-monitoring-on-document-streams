﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="TopkMonitoring.index" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  
     <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <title>Topk Monitoring | Registeration</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/font-awesome.css" rel="stylesheet" />
    <link href="css/custom.css" rel="stylesheet" />
    <link href="css/animate.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet" />


</head>
<body class="gray-bg">
    <form id="form1" runat="server">
     <div class="middle-box text-center loginscreen   animated fadeInDown">
        <div>
            <div>

                <h1 class="logo-name">Topk</h1>

            </div>
            <h3>Register to Topk Monitoring</h3>
                <div class="form-group">
                    <asp:TextBox ID="txt_name" class="form-control" placeholder="Name"  runat="server"></asp:TextBox>
                    <asp:Label ID="lb_name" runat="server" Text="Label"></asp:Label>
                </div>
                
                <div class="form-group">
                      <asp:TextBox ID="txt_uname" class="form-control" placeholder="Username" runat="server"></asp:TextBox>
                     <asp:Label ID="lb_uname" runat="server" Text="Label"></asp:Label>
                </div>
                <div class="form-group">
                      <asp:TextBox ID="txt_pass" class="form-control" placeholder="Password" runat="server"></asp:TextBox>
                   <asp:Label ID="lb_pass" runat="server" Text="Label"></asp:Label>
                </div>
             <div class="form-group">
                      <asp:TextBox ID="txt_confirm" class="form-control" placeholder="Confirm Password"   runat="server"></asp:TextBox>
                   <asp:Label ID="lb_confirm" runat="server" Text="Label"></asp:Label>
                 <asp:Label ID="lb_compare" runat="server" Text="Label"></asp:Label>
                </div>
            <asp:Button ID="btn_register"  type="submit" class="btn btn-primary block full-width m-b" runat="server" Text="Register" OnClick="btn_register_Click" />
                <p class="text-muted text-center"><small>Already have an account?</small></p>
                <a class="btn btn-sm btn-white btn-block" href="login.aspx">Login</a>
           
        </div>
    </div>

   
        <script src="js/jquery-3.1.1.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/icheck.min.js"></script>
   
    <script>
        $(document).ready(function(){
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
    </script>

    </form>
</body>
</html>

